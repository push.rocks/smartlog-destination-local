/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartlog-destination-local',
  version: '9.0.1',
  description: 'a smartlog destination targeting the local console'
}
